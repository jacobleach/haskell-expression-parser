{-# LANGUAGE OverloadedStrings #-}

import Control.Applicative ((<|>))
import Data.Attoparsec.Text
import System.Environment
import Data.Text

data Expression = Expression Int Op Expression |
                  Value Int
                  deriving (Show)

data Op = Add | Subtract | Divide | Multiply deriving (Show)

eval :: Expression -> Int
eval (Expression x Add y)       = x + (eval y)
eval (Expression x Subtract y)  = x - (eval y)
eval (Expression x Divide y)    = x `quot` (eval y)
eval (Expression x Multiply y)  = x * (eval y)
eval (Value x)                  = x

parseValue :: Parser Int
parseValue = decimal

parseExpression :: Parser Expression
parseExpression =
       (parseValue >>= \p1 ->
        parseOp >>= \p2 ->
        parseExpression >>= \p3 -> 
        return (Expression p1 p2 p3))

   <|> (parseValue >>= \p1 ->
        return (Value p1))

parseOp :: Parser Op
parseOp = 
        (char '+' >> return Add)
    <|> (char '-' >> return Subtract)
    <|> (char '/' >> return Divide)
    <|> (char '*' >> return Multiply)

test :: String -> IO ()
test string =
    case parseOnly parseExpression (pack string) of
        (Left a) -> print a
        (Right a) -> print (eval a)

main :: IO ()
main = do
    args <- getArgs
    (print (parseOnly parseExpression (pack (args !! 0))))
